from datetime import datetime


class TimeDiff:
    def __init__(self):
        self.start = datetime.now()
        self.end = 0xffff
        self.delta = 0

    def set_start(self, start):
        self.start = start

    def restart(self):
        self.start = datetime.now()

    def t_delta_now(self):
        self.end = datetime.now()
        self.delta = self.end - self.start
        return self.delta

