'''
    Parses DIMACS CNF file format to data
    that we'll pass to algorithms
    Assuming input file is correct, no error checking
'''

class CnfParser:
    def __init__(self, filename):
        self.clauses = []
        with open(filename) as f:
            self.raw = f.readlines()
            for line in self.raw:
                if line.startswith('c'):
                    continue
                elif line.startswith('p cnf'):
                    problem = line[len('p cnf '):].split()
                    self.var_count = int(problem[0])
                    self.clause_count = int(problem[1])
                else:
                    clause = line.split()
                    # to get rid of terminating 0
                    clause = clause[:-1]
                    clause = [int(x) for x in clause]
                    tmp = []
                    for x in clause:
                        negation = False
                        if x < 0:
                            negation = True
                            x = -1 * x
                        # To switch from x1...xn to 0 based indices
                        x -= 1
                        tmp.append(tuple([x, negation]))
                    self.clauses.append(tmp)

