#!/usr/bin/python
import sys
import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime
from algos.myga import MySatGa
from algos.bruteforce import BruteForceSAT as bfsat
from cnf_parser import CnfParser
from time_diff import TimeDiff

'''
    3-CNF
        Szukanie rozwiązań za pomocą:
            - GA
            - DPLL (nie zaimplementowane :c)
            - Brute Force

        fitness - zwraca liczbę spełnionych klauzul w cnf

    Wywołanie programu:
    python main.py cnf_file run_bf optimize

    cnf_file - plik wejsciowy z rozszerzeniem
    run_bf - jeśli nie 0 to uruchamiany jest również brute force,
             niepotrzebny w czasie testowania GA
    optimize - 0/1 - informuje program czy należy zwiększać losowość
               w przypadku utknięcia w nieoptymalnym rozwiązaniu
'''
def fitness(individual, data):
    clauses_satisfied = 0

    for clause in data:
        # print(clause)
        # print(individual)
        # print(len(individual))
        x0 = individual[clause[0][0]]
        x1 = individual[clause[1][0]]
        x2 = individual[clause[2][0]]
        # print('A: {} {} {}'.format(x0, x1, x2))
        x0 ^= clause[0][1]
        x1 ^= clause[1][1]
        x2 ^= clause[2][1]
        # print('B: {} {} {}'.format(x0, x1, x2))

        # at least one must be 1 to satisfy the clause
        result = x0 | x1 | x2
        # print('result {}'.format(result))
        if result != 0:
            clauses_satisfied += 1
        # print('satisified: {}'.format(clauses_satisfied))
        # input()
    return clauses_satisfied

def fitness_score(individual, data):
    score = 0

    for clause in data:
        x0 = individual[clause[0][0]]
        x1 = individual[clause[1][0]]
        x2 = individual[clause[2][0]]

        x0 ^= clause[0][1]
        x1 ^= clause[1][1]
        x2 ^= clause[2][1]

        result = x0 + x1 + x2
        score += result

    return score


if __name__ == "__main__":
    try:
        cnf_file = './cnf/' + sys.argv[1]
        print(cnf_file)
        run_bf = int(sys.argv[2])
        optimize = int(sys.argv[3])
    except IndexError:
        print('Podaj poprawne wejscie')

    cnf = CnfParser(cnf_file)
    print('Klauzul: {}\nZmiennych: {}'.format(cnf.clause_count, cnf.var_count))

    # Create timepoint
    td = TimeDiff()

    # Solving with brute force
    if run_bf != 0:
        bf = bfsat(cnf.clauses, cnf.clause_count, cnf.var_count)
        bf.solve()
        print(bf.solutions)
        td.t_delta_now()
        bf.finalize(str(td.delta))

    # Solving with DPLL algorithm
    # dpll = Dpll(cnf.clauses, cnf.var_count)
    # dpll.solve()

    # Solving with genetic algorithm
    ga = MySatGa(cnf.clauses, cnf.var_count)
    ga.fitness_function = fitness
    td.restart()
    ga.run()
    td.t_delta_now()
    ga.finalize('ga.log', str(td.delta), True)
    # ga.print_best_history()
    # print(ga.best_individual_to_int())

    # ga_score = MySatGa(cnf.clauses, cnf.var_count)
    # ga_score.fitness_function = fitness_score
    # ga_score.max_score = 3 * len(cnf.clauses)
    # td.restart()
    # ga_score.run()
    # td.t_delta_now()
    # ga_score.finalize('ga_score.log', str(td.delta), True)
    # print(ga_score.best_individual_to_int())

    # Some charts:
    # print(ga.population_size)
    # print(ga.generations)
    # print(ga.crossover_probability)
    # print(ga.mutation_probability)
    # print(ga.elitism)
    # print(ga.maximise_fitness)
    # generations = list(range(ga.generations - 1))
    # fitness_data = [i[0] for i in ga.best_history]

    # fig = plt.figure()
    # ax = fig.add_subplot(1,1,1)
    # ax.plot(generations, fitness_data, '-r', label = 'best fitness')
    # ax.plot([0, ga.generations - 2], [cnf.clause_count, cnf.clause_count], '-b', label = 'max fitness')
    # ax.set(xlabel='Pokolenie', ylabel='Wartosc funcji fitness',
    #        title='Fitness najlepszego chromosomu - {}, n = {}'.format(ga.best_individual_to_int(), cnf.var_count))
    # axes = plt.gca()
    # axes.set_ylim([cnf.clause_count / 2, 1.1 * cnf.clause_count])
    # ax.grid()
    # plt.legend()
    # plt.show()

    # tuning randomization
    del ga
    probabilities = []
    results = []
    result = 0
    i = 0
    j = 0
    best = 0
    while i < 21:
        probabilities.append(i * 0.05)
        # for given probability loop until we find max or until 50 tries
        while j < 10:
            ga = MySatGa(cnf.clauses, cnf.var_count)
            ga.mutation_probability = 0.05 * i
            ga.crossover_probability = 0.05 * i
            ga.fitness_function = fitness
            ga.run()
            j += 1
            best += ga.best_individual()[0]
            # delete explicitly to make sure we create GA object from scratch
            del ga
            print(j)
        results.append(best / 10)
        best = 0
        j = 0
        i += 1
        print(i)



    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.plot(probabilities, results, '-ro', label = 'best fitness')
    ax.plot([0, 1], [cnf.clause_count, cnf.clause_count], 'b', label='max fitness')
    ax.set(xlabel='Prawdopodobienstwo mutacji i crossoveru', ylabel='Wartosc funcji fitness',
          title='Średni fitness najlepszego chromosomu w zależności od prawdopodobieństwa, zmiennych n = {}'.format(cnf.var_count))
    axes = plt.gca()
    axes.set_ylim([cnf.clause_count - 10, 1.03 * cnf.clause_count])
    ax.grid()
    plt.legend()
    plt.show()


