import os


class BruteForceSAT:
    def __init__(self, data, clause_cnt, var_cnt):
        self.clauses = data
        self.clause_cnt = clause_cnt
        self.var_cnt = var_cnt
        self.solutions = []
        try:
            os.remove('bf.log')
        except FileNotFoundError:
            pass

    def finalize(self, time_delta):
        with open('bf.log', 'a') as bflog:
            bflog.write('Brute force took: ' + str(time_delta) + '\n')

    def solve(self):
        # so all bits get set eventually
        tests = 2 ** self.var_cnt - 1
        i = 0
        for x in range(0, tests):
            # print('Testing x = {}'.format(x))
            i += 1
            if i == 10000000:
                print('Testing {}%'.format(100 * x / tests))
                i = 0
            break_ocurred = False
            for clause in self.clauses:
                # i += 1
                # print(i)
                # print(clause)
                idx0 = clause[0][0]
                negx0 = clause[0][1]
                idx1 = clause[1][0]
                negx1 = clause[1][1]
                idx2 = clause[2][0]
                negx2 = clause[2][1]

                # extract proper bits and negate if clause says so
                # x    | neg   | res
                # 0    | 0     | 0 - dont neg 0
                # 0    | 1     | 1 - neg 0
                # 1    | 0     | 1 - dont neg 1
                # 1    | 1     | 0 - neg 1 - leads to XOR
                x0 = (x & (1 << idx0)) >> idx0
                x0 ^= negx0
                x1 = (x & (1 << idx1)) >> idx1
                x1 ^= negx1
                x2 = (x & (1 << idx2)) >> idx2
                x2 ^= negx2

                # check for failure or record passing solution:
                result = x0 | x1 | x2

                # debug
                # print(clause)
                # print("{} {} {}".format(x0, x1, x2))
                # print('result: {}'.format(result))
                # # print("{} {} {}".format(negx0, negx1, negx2))
                # input()

                if result == 0:
                    break_ocurred = True
                    break

            # if we didn't break for given value, we found a solution
            if break_ocurred == False:
                self.solutions.append(x)
                with open('bf.log', 'a') as bflog:
                    bflog.write('Found solution: {}\n'.format(bin(x)))

        with open('bf.log', 'a') as bflog:
            bflog.write('Summary: {}\n'.format(self.solutions))
            bflog.write('Brute force - found {} solutions'.format(len(self.solutions)) + '\n')
