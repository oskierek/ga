from pyeasyga import pyeasyga
import random
import os


class MySatGa(pyeasyga.GeneticAlgorithm):
    def __init__(self, data, var_count):
        super().__init__(data)

        # we pass cnf clauses as data but it generates individual
        # of len(data) and we just need that much as there are
        # variables
        def create_individual_override(seed_data):
            return [random.randint(0, 1) for _ in range(var_count)]

        self.create_individual = create_individual_override
        self.max_score = len(data)
        self.best_history = []
        self.optimize = False

    def create_next_generation(self):
        super().create_next_generation()
        # gather best individuals data to make some charts
        self.best_history.append(self.best_individual())

    def print_best_history(self):
        i = 0
        for individual in self.best_history:
            print(i)
            i+=1
            print(individual)

    def best_individual_to_int(self):
        best = self.best_individual()
        best_int = 0

        for idx in range(len(best[1])):
            if(best[1][idx] != 0):
                best_int |= (1 << idx)

        return best_int

    def finalize(self, log_file, time_delta, override_log):
        if override_log:
            try:
                os.remove(log_file)
            except FileNotFoundError:
                pass

        with open(log_file, 'a') as galog:
            # print(self.best_individual())
            # Check if program found individual fullfilling cnf
            if self.best_individual()[0] == self.max_score:
                galog.write("Found solution for all clauses {}\n".format(self.best_individual()))
            else:
                galog.write("Did not find proper solution, best one: {}\n".format(self.best_individual()))
            galog.write("Genetic algo took: " + str(time_delta))
