\documentclass{scrartcl}

\usepackage{ulem}
\usepackage{hyperref}
\usepackage[T1]{fontenc}
\usepackage[polish]{babel}
\usepackage[utf8]{inputenc}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,
    urlcolor=cyan,
}

\usepackage{listings}
\usepackage{xcolor}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2
}

\usepackage{graphicx}
\graphicspath{ {../plots/} }

\begin{document}
\title{Inteligencja Obliczeniowa projekt 1}
\subtitle{Wykorzystanie algorytmu genetycznego do sprawdzenia spełnialności formuły 3-CNF}
\author{Oskar Bagiński 273519}
\date{27.11.2020}
\maketitle

\pagebreak

\section{Wprowadzenie}

Celem projektu jest porównanie czasu rozwiązania problemu spełnialności klauzuli 3-CNF z pomocą algorytmu genetycznego
i porównanie czasu wykonania z algorytmami brute-force \sout{i DPLL}. \\
Sprawozdanie zostało sporządzone w środowisku \LaTeX.\\
Projekt ma nastpępującą strukturę:
\begin{itemize}
    \item{algos} - klasy implementujące opisane algorytmy
    \item{cnf} - pliki wejściowe dla programu
    \item{docs} - folder z plikami tekstowymi sprawozdania
    \item{plots} - wygenerowane przebiegi opisane w sprawozdaniu
    \item{cnf\_parser.py} - moduł przetwarzający pliki w formacie \textbf{.cnf} na struktury programu
    \item{main.py} - główna funkcja programu rozwiązującego problem
    \item{time\_diff.py} - klasa pomocnicza do obliczania czasów wykonania
\end{itemize}

\section{Implementacja}
Poniżej przedstawiono założenia programistyczne i szczegóły rozwiązania przedstawionego zadania.

\subsection{Środowisko}
Projekt napisany jest w języku \textbf{Python} z wykorzystaniem gotowego pakietu \textbf{pyeasyga} będącego gotową implementacją
algorytmu genetycznego. Kilka z funkcji jest dziedziczonych i prystosowanych na potrzeby zadania i zebrania wyników do sprawozdania.
Pozostałe części projektu wraz z funkcjami fitness, implementacją brute-force i parsowaniem wejścia zostały napisane własnoręcznie.

\subsubsection{Input}
Wejściem dla algorytmu w przypadku tej implementacji są pliki \textbf{.cnf} w formacie DIMACS. Nie są one generowane przez autora
- są pobierane ze stron rekomendowanych w instrukcji do zadania:
\begin{itemize}
    \item{\href{https://www.cs.ubc.ca/~hoos/SATLIB/benchm.html}{https://www.cs.ubc.ca/~hoos/SATLIB/benchm.html}}
    \item{\href{https://people.sc.fsu.edu/~jburkardt/data/cnf/cnf.html}{https://people.sc.fsu.edu/~jburkardt/data/cnf/cnf.html}}
\end{itemize}

\subsubsection{Parsowanie wejściowego pliku}
Pliki \textbf{.cnf} przetwarzane są w programie do formy listy list trzech krotek:\\
\begin{lstlisting}[language=Python, caption=CNF data format]
    cnf = [
            [(1, True), (2, True), (4, False)]
            [(6, True), (5, True), (14, False)]
            ...
            [(4, True), (n-2, True), (n-1, False)]
          ]
\end{lstlisting}

Pierwszym elementem w każdej krotce jest indeks zmiennej logicznej x\textsubscript{n} w zakresie \textbf{[0, n-1]} występującej
w adekwatnej klauzuli.
Jako że w standardzie DIMACS liczba \textbf{0} jest znakiem końca lini, indeksy zmiennych logicznych x\textsubscript{n} zaczynają się od 1.
W celu ułatwienia sobie pracy, indeksy te są od razu zmniejszane o 1 aby rozpoczynały się od zera jako że konieczne będzie operowanie
na elementach list.Pierwszym elementem krotki jest indeks danej zmiennej, zaczynając od zera. \\
Drugim elementem krotki jest wartość Boolean - przyjmuje wartość False, gdy zmienna nie ma być negowana w danej klauzuli oraz wartość
True w przypadku jej negacji. Dobranie takich parametrów prowadzi do następującej tabeli prawdy:

\begin{center}
 \begin{tabular}{||c c c c||}
 \hline
    x\textsubscript{n} & Neg & Wynik & Komentarz \\ [0.5ex]
 \hline\hline
    0 & 0 & 0 & Nie neguj logicznego 0 \\
 \hline
    0 & 1 & 1 & Zaneguj wartość logiczną 0 \\
 \hline
    1 & 0 & 1 & Nie neguj wartości logicznej 1 \\
 \hline
    1 & 1 & 0 & Zaneguj wartość logiczną 1 \\
 \hline
\end{tabular}
\end{center}

Z powyższej tabeli widać, że jeśli chce się uzyskać aktualną wartość danej klauzuli należy sięgnąc po wartość logiczną
chromosomu, wskazywaną przez pierwszy elementy krotki i dokonać operacji \textbf{XOR} z drugim argumentem. Wówczas wartość
całej klauzuli może być obliczona poprzez dokonanie bitowej operacji \textbf{OR} na wartościach zmiennych w klauzuli.\\

Lista ta ani krotki w niej nie są w żaden sposób porządkowane czy sortowane, a dostarczane algorytmom w tej samej kolejności co w pliku wejściowym.\\
Główna lista symbolizuje logiczną formułę. Każda z list krotek tożsama jest z poszczególną klauzulą w podanej
formule logicznej.

\subsection{Chromosom}
W opisywanej implementacji dla klauzuli o \textit{n} zmiennych logicznych chromosom przyjmuje formę listy \textit{n}
wartości \textbf{int} z zakresu \textbf{[0, 1]} - takie rozwiązanie było prostsze przy wykorzystaniu biblioteki \textbf{pyeasyga}
i przystosowaniu do własnych potrzeb. Gdyby próbować użyć n-bitowej liczby zamiast listy, łatwo przekroczyć 64-bitowy
rozmiar zmiennej, co skutkowało wyjątkiem podczas uruchomienia programu dla wejścia o sporej liczbie zmiennych logicznych.
W tym wypadku można by generować tablicę takich liczb w celu zaoszczędzenia pamięci, jednak powoduje to konieczność
sprawdzania większej ilości warunków i korzystania z większej liczby operacji bitowych. W związku z tym, uznając rozmiar chromosomu
za mimo to akceptowalny, postanowiono pozostać przy formie listy.

\subsection{Funkcje fitness}
Oczywistą propozycją dla funkcji fitness jest policzenie ilości spełnianych przez dany chromosom klauzul w testowanej formule.
Ponadto przetestowana zostanie funkcja przydzielająca chromosomowi maksymalnie 3 * n punktów w zależności od tego, ile zmiennych
i ich negacji daje wartość True.

\begin{itemize}
    \item{\textit{fitness}} - ta funkcja zwraca dla danego chromosomu ilość spełnianych klauzul,
    \item{\textit{fitness\_score}} - zwraca ilość zmiennych lub ich negacji w klauzuli dających wartość True
\end{itemize}

Wykorzystana prosta funkcja fitness zaprezentowana jest na poniższym listingu. Głównie ta wersja została
użyta w badaniach, ponieważ funkcja przyznająca punkty chromosomowi za nadanie danym wartościom logicznym lub ich negacjom
wartości True ma tendencję do utknięcia przy punktacji znacznie poniżej maksymalnej.\\

\begin{lstlisting}[language=Python, caption=Funkcja fitness]
def fitness(individual, data):
    clauses_satisfied = 0

    for clause in data:
        x0 = individual[clause[0][0]]
        x1 = individual[clause[1][0]]
        x2 = individual[clause[2][0]]

        x0 ^= clause[0][1]
        x1 ^= clause[1][1]
        x2 ^= clause[2][1]

        # at least one must be 1 to satisfy the clause
        result = x0 | x1 | x2
        if result != 0:
            clauses_satisfied += 1

    return clauses_satisfied
\end{lstlisting}

\subsection{Brute force}
Algorytm brute force zaimplementowany własnoręcznie w programie jest bardzo prosty: badana jest ilość zmiennych n, a następnie dla x w zakresie
\textbf{[0, 2\textsuperscript{n} - 1]} wartości logiczne są obliczane zgodnie z logiką funkcji fitness. Różnica jest jedynie w tym, że wejście
ma formę n-bitowej liczby zamiast tablicy. W związku z tym zamiast indeksowania wykorzystane są operacje bitowe. Algorytm ten nie zatrzymuje
się w przypadku znalezienia rozwiązania, sprawdza każdą kolejną możliwość i poprawne dodaje do wyjściowej listy.

\begin{lstlisting}[language=Python, caption=Brute force]
def solve(self):
        # so all bits get set eventually
        tests = 2 ** self.var_cnt - 1

        for x in range(0, tests):
            break_ocurred = False
            for clause in self.clauses:
                idx0 = clause[0][0]
                negx0 = clause[0][1]
                idx1 = clause[1][0]
                negx1 = clause[1][1]
                idx2 = clause[2][0]
                negx2 = clause[2][1]

                # extract proper bits and negate if clause says so
                # x    | neg   | res
                # 0    | 0     | 0 - dont neg 0
                # 0    | 1     | 1 - neg 0
                # 1    | 0     | 1 - dont neg 1
                # 1    | 1     | 0 - neg 1 - leads to XOR
                x0 = (x & (1 << idx0)) >> idx0
                x0 ^= negx0
                x1 = (x & (1 << idx1)) >> idx1
                x1 ^= negx1
                x2 = (x & (1 << idx2)) >> idx2
                x2 ^= negx2

                # check for failure or record passing solution:
                result = x0 | x1 | x2

                if result == 0:
                    break_ocurred = True
                    break

            # if we didn't break for given value, we found a solution
            if break_ocurred == False:
                # logging solution here
\end{lstlisting}

\section{Badania i optymalizacje}
W kolejnych podpunktach podsumowane będą poczynione badania i obserwacje. Nie będą tu poruszane przypadki podania jako wejście do programu
klauzuli niespełnialnej.

\subsection{Czas wykonania}
W poniższej tabeli porównano czas wykonania algorytmu genetycznego i brute force dla trzech przypadków:
\begin{itemize}
    \item{20 zmiennych, 91 klauzul}
    \item{50 zmiennych, 80 klauzul}
    \item{75 zmiennych, 325 klauzul}
\end{itemize}
Są to czasy egzekucji algorytmu, bez względu czy w konkretnym uruchomieniu algorytm znalazł rozwiązanie.

\begin{center}
 \begin{tabular}{||c c c||}
 \hline
    Ilość zmiennych & Czas wykonania GA & Czas wykonania BF \\ [0.5ex]
 \hline\hline
    20 & 0:00:00.324963 s & 0:00:05.817465 s \\
 \hline
    50 & 0:00:00.346045 s & (obliczone) 31709.79 dni \\
 \hline
    75 & 0:00:00.964930 s & N/A \\
 \hline
\end{tabular}
\end{center}

\subsection{Przebiegi}
Poniżej umieszczono przebiegi wartości fitness dla n - ilości zmiennych logicznych oraz c - danej ilości klauzul.
Generalna zasada jest taka dla podanych parametrów algorytmu genetycznego:\\
\begin{lstlisting}[language=Python, caption=Parametry algorytmu genetycznego]
ga = MySatGa(data,
        population_size=50,
        generations=100,
        crossover_probability=0.8,
        mutation_probability=0.2,
        elitism=True,
        maximise_fitness=True)
\end{lstlisting}

\subsubsection {n = 20, c = 91}
Poniżej przedstawione są 3 przypadki dla tych samych parametrów.\\
\includegraphics[scale=0.4]{20_91_1}
\includegraphics[scale=0.4]{20_91_2}
\includegraphics[scale=0.4]{20_91_3}

\subsubsection {n = 50, c = 80}
\includegraphics[scale=0.4]{50_80_1}
\includegraphics[scale=0.4]{50_80_2}

\subsubsection {n = 75, c = 325}
\includegraphics[scale=0.4]{75_325_1}
\includegraphics[scale=0.4]{75_325_2}

\subsection{Fitness w zależności od losowości}
Poniżej przedstawiono wykres pokazujący jak wpływa losowość na wartość funkcji fitness.
Dane do tego wykresu zostały wygenerowane w następujący sposób:\\
\begin{lstlisting}[language=Python, caption=Wpływ randomizacji na fitness]
    mean_vals = []
    probabilities = []
    i =0, j = 0
    acc = 0
    probabilities.append(i * 0.05)
    for i = 0 -> 20
        for j = 0 -> 10
            GA.mutation = 0.05 * i
            GA.crossover = 0.05 * i
            wykonaj GA
            acc = acc + best_fitness
        mean_vals.append(acc / 10)
        j = 0, acc = 0

\end{lstlisting}

Są to średnie wartości funkcji fitness w dziesięciu przebiegach dla każdej wartosci prawdopodobienstwa mutacji i krzyżowania.
Zamieszczone zostały tutaj wykresy po dwa przebiegi dla dwóch zestawów danych:
\begin{itemize}
    \item{50 zmiennych, 80 klauzul}
    \item{75 zmiennych, 325 klauzul}
\end{itemize}

Widać na nich, że bez jakiejkolwiek randomizacji wyniki te są najdalsze rzeczywistemu rozwiązaniu danego problemu i
zbliżają się do niego wraz ze wzrostem losowości. Zjawisko to widać zwłaszcza w przypadku większego zestawu danych.\\
Na poniższych wykresach wkradł się błąd w legendzie, zamiast 'best fitness' powinno być 'mean fitness'.

\subsubsection {n = 50, c = 80}
\includegraphics[scale=0.4]{rand_50_80_1}
\includegraphics[scale=0.4]{rand_50_80_2}

\subsubsection {n = 75, c = 325}
\includegraphics[scale=0.4]{rand_75_325_1}
\includegraphics[scale=0.4]{rand_75_325_2}

\section{Konkluzja}
Algorytmy genetyczne znacznie szybciej zwracają rozwiązanie danego problemu, zwłaszcza w porównaniu z implementacją typu brute force.
Jeśli istotą problemu jest znalezienie nie najlepszego, a w miarę dobrego rozwiązania a do tego wiadomo, jaka jest wartość docelowa,
algorytm genetyczny znacznie szybciej jest w stanie uporać się z problemem. Ponadto widać że jego czas wykonania rośnie znacznie wolniej
wraz z rozmiarem wejścia niż w przypadtku prostej implementacji siłowej. Szczególnie szybko rósł czas wykonania
algorytmu typu brute force wraz ze wzrostem ilości zmiennych co nie powinno dziwić, gdyaż zależność ta jest potęgowa.
Wystarczyło podwoić rozmiar wejścia, by czas wykonania algorytmu brute force wzrósł na tyle, że trzeba było obliczyć czas jego
wykonania ponieważ czekanie na zakończenie byłoby bezsenowne poczas gdy wykonanie algorytmu genetycznego wzrosło nieznacznie.
\end{document}
